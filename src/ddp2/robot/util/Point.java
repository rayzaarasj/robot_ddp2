package ddp2.robot.util;

/**
 * Created by wisnuprama on 3/1/2017.
 */

/**
 * point untuk sistem koordinat
 * NO STATIC METHOD HERE, mengurangi kebingungan wkwk
 * */
public class Point {
    // for display only
    private int coordX;
    private int coordY;

    // for real implementation
    private int coordXmap;
    private int coordYmap;

    public Point(){
        this.coordX = 0;
        this.coordY = 0;

        this.coordXmap = 0;
        this.coordYmap = 0;
    }

    public Point(int coordX, int coordY) {
        this.coordX = coordX;
        this.coordY = coordY;

        this.coordXmap = coordX;
        this.coordYmap = coordY;
    }

    public Point getPosition(){
        return new Point(coordXmap, coordYmap);
    }

    public Point getKartesianPosition(){
        return new Point(coordX, coordY);
    }

    public int getCoordXmap() {
        return coordXmap;
    }

    public void setCoordXmap(int coordX) {
        this.coordXmap = coordX;
    }

    public int getCoordYmap() {
        return coordYmap;
    }

    public void setCoordYmap(int coordY) {
        this.coordYmap = coordY;
    }

    public void setPosition(int coordX, int coordY){
        this.coordXmap = coordX;
        this.coordYmap = coordY;
    }

    public void translate(int coordX, int coordY){
        this.coordX += coordX;
        this.coordY += -coordY;

        this.coordXmap += coordX;
        this.coordYmap += coordY;
    }

    public void moveCoordX(int coordX){
        this.coordX += coordX;
    }

    public void moveCoordY(int coordY){
        this.coordY += coordY;
    }

    @Override
    public String toString(){
        // representation in infinite map
        return String.format("X:%d, Y:%d", this.coordX, this.coordY);
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }
}
