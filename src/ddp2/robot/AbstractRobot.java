package ddp2.robot;

import ddp2.map.BidangKotak;
import ddp2.robot.util.Moment;
import ddp2.robot.util.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * AbstractRobot
 * Basic implementation
 */

public abstract class AbstractRobot
extends Point {
    // CONST
    private final int MOVE_ONE_STEP = 1; // cuma boleh satu step
    private final char DEFAULT_DIRECTION = 'E'; // default head direction ke timur

    private String nameRobot;
    protected List<Moment> memoryRobot = new ArrayList<Moment>();
    private BidangKotak map;

    public AbstractRobot(String nameRobot, BidangKotak map){
        super(0,0 );
        this.nameRobot = nameRobot;
        this.map = map;

        // construct first memory
        this.setDirection();
    }

    public AbstractRobot(String namaRobot, BidangKotak map, int coordX, int coordY) {
        // superclass constructor
        // coordinate must from 0-something
        super(coordX, coordY);
        this.nameRobot = namaRobot;
        this.map = map;

        // construct first memory
        this.setDirection();
    }

    //  NAVIGATION
    public void putRobot(){
        // put robot in 0,0 position
        // map get nested list then set robot pos by y index
        this.setPosition(this.getCoordXmap(),this.getCoordYmap());
        map.setPosition(this);
    }

    public void turnLeft(){
        // turn left

        // set direction
        switch (this.getDirection()){
            case 'N':
                this.setDirection('W');
                break;
            case 'S':
                this.setDirection('E');
                break;
            case 'E':
                this.setDirection('N');
                break;
            case 'W':
                this.setDirection('S');
                break;
            default:
                this.setDirection(this.getDirection());
        }

    }

    public void turnRight(){
        // turn left

        // set direction
        switch (this.getDirection()){
            case 'N':
                this.setDirection('E');
                break;
            case 'S':
                this.setDirection('W');
                break;
            case 'E':
                this.setDirection('S');
                break;
            case 'W':
                this.setDirection('N');
                break;
            default:
                // moment bertambah tapi tidak bergerak
                this.setDirection(this.getDirection());
        }
    }

    public void move(){
        /*
        * add to memory first
        * then change ....
        * */

        // translate robot
        switch (this.getDirection()){
            case 'N':
                this.translate(0, -MOVE_ONE_STEP);
                // add to memory
                this.addMoment(this.getDirection());
                break;
            case 'S':
                this.translate(0, MOVE_ONE_STEP);
                // add to memory
                this.addMoment(this.getDirection());
                break;
            case 'E':
                this.translate(MOVE_ONE_STEP, 0);
                // add to memory
                this.addMoment(this.getDirection());
                break;
            case 'W':
                this.translate(-MOVE_ONE_STEP, 0);
                // add to memory
                this.addMoment(this.getDirection());
                break;
            default:
                // no default
        }
        // set robot's position in map
        map.setPosition(this);

    }

    public void back(){

    }

    @Override
    public String toString(){
        // String repr
        StringBuilder str = new StringBuilder();

        switch (this.getDirection()) {
            case 'N':
                str.append("(^^)");
                break;
            case 'S':
                str.append("(vv)");
                break;
            case 'E':
                str.append("(>>)");
                break;
            case 'W':
                str.append("(<<)");
                break;
            default:
        }

        return str.toString();
    }

    public String getNameRobot() {
        return this.nameRobot;
    }

    public char getDirection(){
        /*
        * jika memory masih belum ada, berarti belum terjadi
        * pergantian arah sehingga gunakan yang terakhir.
        * ------
        * implementasi untuk undo
        * */

        // belum terjadi moment apa-apa
        if (this.memoryRobot.isEmpty())
            return DEFAULT_DIRECTION;

        Moment tmpMoment = memoryRobot.get(memoryRobot.size() - 1);
        return tmpMoment.getHistoryDirection();
    }

    protected void setDirection(){
        /* set to default
        *  the default is E
        */
        this.addMoment(DEFAULT_DIRECTION);
    }

    protected void setDirection(char direction){
        // set by user
        this.addMoment(direction);
    }

    private void addMoment(char direction){
        // add moment to memory when robot change direction
        int tmpX = (this.getCoordXmap() + map.getSizeX()) % map.getSizeX();
        int tmpY = (this.getCoordYmap() + map.getSizeY()) % map.getSizeY();

        // update the coordinate too
        this.setPosition(tmpX, tmpY);

        Point tmpLocation = new Point(tmpX, tmpY);
        this.memoryRobot.add(new Moment(tmpLocation, direction));

    }

    public Moment getMoment(){
        // get last moment
        // MOMENT DIJAMIN ADA SATU
        if (memoryRobot.size() > 1)
            return memoryRobot.get(memoryRobot.size() - 1);
        return memoryRobot.get(0);
    }

    @Override
    public Point getPosition(){
        // get last position
        Moment nowMoment = getMoment();
        return nowMoment.getHistoryPoint();
    }

    public Moment getOldMoment(){
        // get one moment before last moment
        // MOMENT DIJAMIN ADA SATU
        if (memoryRobot.size() > 1)
            return memoryRobot.get(memoryRobot.size() - 2);
        return memoryRobot.get(0);
    }

    public Point getOldPosition(){
        // get old point from old moment
        Moment oldMoment = getOldMoment();
        return oldMoment.getHistoryPoint();
    }

}
